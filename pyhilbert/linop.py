"""
linop.py

Author: James Ashton Nichols
Start date: October 2018 

The abstraction of some linear algebra in Hilbert spaces for doing functional 
analysis computations, where the class "Basis" does some predictable operations 
on the class "Vector", which has to have a dot product defined as well as 
typical linear algebra.
"""

import math
import numpy as np
import scipy as sp
import warnings

from pyhilbert.pyhilbert.space import *

__all__ = ['LinOp'] 

"""
class Element(object):
Some sort of generic base class for all elements?

Including A / T
Including spaces
Including arithmetic
"""

class LinOp(object):
    """ 
    Consider the linear operator A : X -> Y

    Here we say X is the "right" space, or r_space and Y is the "left" space. 
    This nomenclature comes from the fact that Ax is the usual way to write 
    "A applied to x", and since Ax is in Y, for any y in Y we can write
    < y, Ax > for the inner product in Y, hence Y is a "left" space or l_space.

    Routines available include:

    matrix_multiply(A)
    """

    def __init__(self, values=None, axes=None):
         
        # vecs is considered to be a (n x d) array, where n is the number of vectors 
        # and d is the dimension of the space, or a list of vectors
        if axes is None:
            self.axes = (l2,) * values.ndim
        else:
            self.axes = axes

        self._set_values(values)

    @property
    def shape(self):
        return self._values.shape
    
    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, values):
        self._set_values(values)

    def _set_values(self, values):
        if values is not None:
            if len(self.axes) != values.ndim:
                raise ValueError(f'{self.__class__.__name__}: value array ' \
                                 f'not of correct dimension {values.ndim} for num axes ' \
                                 f'{len(self.axes)}')

            for axis_i, (space, n) in enumerate(zip(self.axes, values.shape)):
                if space.check_values_shape(values, axis_i): 
                    # This does some fancy boundary condition maintenance
                    values = space.values_format(values, axis_i)
                else:
                    raise ValueError(f'{self.__class__.__name__}: value array ' \
                                     f'not of correct size {values.shape} for axes ' \
                                     f'{[str(ax) for ax in self.axes]}')

        self._values = values

    @property
    def is_diagonal(self):
        # implement this
        pass
    
    @property
    def A(self):
        return type(self)(values = self.values.T, axes = self.axes[::-1])
    # ALSO HAVE .T TO FIT IN LINE WITH NUMPY 
    @property
    def T(self):
        return type(self)(values = self.values.T, axes = self.axes[::-1])
 
    def __str__(self):
        string = str(self.__class__.__name__) + '(' + str(self.values.shape) + ', ('
        for ax in self.axes[:-1]:
            string += str(ax) + ', '
        string += str(self.axes[-1]) + '))'
        return string

    # Consider implementing this - not the worst idea
    def __getitem__(self, idx):
        return self.values[idx]

    def __setitem__(self, idx, vals):
        self._values[idx] = vals
        # This procedure at least sends the values array through the setter routine
        self.values = self._values

    def __matmul__(self, other):        
        return self.matmul(other)

    def matmul(self, other, axis=None):

        # NOTE: TO consider: What if other is a scalar and fits with this LinOp?
        # In that case the @ for the np.ndarrays underlying fails

        if hasattr(other, 'axes') and hasattr(other, 'values'):
            if self.axes[-1].is_dot_compatible(other.axes[0]):
                # Then other is a member of the hilbert space family
                res_vals = self.axes[-1].values_dot(self.values, other.values)
                # The tuple of axes are concatenated with the middle two removed
                res_axes = self.axes[:-1] + other.axes[1:]
            else:
                raise ValueError(f'{self.__class__.__name__}: incompatible RHS {str(other)} given to matmul()')

        elif self.axes[-1].is_dot_compatible(l2):
            # We assume other is array-like
            res_vals = self.axes[-1].values_dot(self.values, np.atleast_1d(other))
            # The tuple of axes are concatenated with the middle two 
            res_axes = self.axes[:-1] + (l2,) * (other.ndim - 1)
        
        else:
            raise ValueError(f'{self.__class__.__name__}: unrecognised RHS given to matmul()')
        
        return self._build_from_matmul(other, res_vals, res_axes)

    def _build_from_matmul(self, other, res_vals, res_axes):

        if hasattr(res_vals, 'ndim'):
            if all([ax == l2 for ax in res_axes]):
                # Then return an ndarray
                return res_vals

            # I don't think we need this as a special case... commented out for now...
            #if res_vals.ndim == 1 and res_vals.shape[0] == 1 or res_vas.shape[0] == 0: 
            #    # Then it's a singleton! 
            #    return res_vals
            
            # But the base case is to return a LinOp 
            return LinOp(values=res_vals, axes=res_axes)
        else:
            # Then it's some uncaught singleton
            warnings.warn(f'{self.__class__.__name__}: Unexpected singleton from {str(self)} matmul {str(other)}')
            return res_vals
            
    def __rmatmul__(self, other):
        return self.rmatmul(other)

    def rmatmul(self, other, axis=None):
        
        if hasattr(other, 'axes') and hasattr(other, 'values'):
            if self.axes[0].is_dot_compatible(other.axes[-1]):
                # Then other is a member of the hilbert space family
                res_vals = self.axes[0].values_dot(other.values, self.values)
                # The tuple of axes are concatenated with the middle two removed
                res_axes = other.axes[:-1] + self.axes[1:]
            else:
                raise ValueError(f'{self.__class__.__name__}: incompatible LHS {str(other)} given to rmatmul()')

        elif self.axes[0].is_dot_compatible(l2):
            # We assume other is array-like
            res_vals = self.axes[0].values_dot(other, self.values)
            # The tuple of axes are concatenated with the middle two removed
            res_axes = (l2,) * (other.ndim - 1) + self.axes[1:]
        
        else:
            raise ValueError(f'{self.__class__.__name__}: unrecognised LHS {str(other)} given to matmul()')

        return self._build_from_matmul(other, res_vals, res_axes)

    def dot(self, other, axis=None):
        return self.matmul(other)

    # Consider implementing Frobenius and all the other norms...
    def norm(self, axis=None):

        # We compute the norm along a specified axis. If not present, then we
        # collapse all axes using their appropriate inner products
        # TO BE IMPLEMENTED!

        pass

    def _check_compat(self, other):
        if hasattr(other, 'axes') and hasattr(other, 'values'):
            return other.axes == self.axes
        else: 
            raise ValueError(f'{self.__class__.__name__}: space {self} mismatches space {other}')
    
    # This ensures that np.ndarray operators don't try to operate on these
    # object types. Ensures the binary operators defined below are executed. 
    # Possible TODO: Define this function and redefine the creation routines,
    # so we could inherit all ufuncs from np.lib.mixins.NDArrayOperatorsMixin
    __array_ufunc__ = None

    def __add__(self, other):
        if self._check_compat(other):
            return type(self)(self.values + other.values, axes=self.axes)
                        
    __radd__ = __add__

    def __iadd__(self, other):
        if self._check_compat(other):
            self._values += other.values
            return self

    def __sub__(self, other):
        if self._check_compat(other):
            return type(self)(self.values - other.values, axes=self.axes)

    def __rsub__(self, other):
        if self._check_compat(other):
            return type(self)(other.values - self.values, axes=self.axes)

    def __isub__(self, other):
        if self._check_compat(other):
            self._values -= other.values
            #self._vec_is_new[:] = True
            return self

    def __neg__(self):
        return type(self)(-self._values, axes=self.axes)

    def __pos__(self):
        return type(self)(+self._values, axes=self.axes)

    def __mul__(self, other):
        return type(self)(self._values * other, axes=self.axes)
   
    def __rmul__(self, other):
        return type(self)(other * self._values, axes=self.axes) 

    def __imul__(self, other):
        self.values *= other
        return self

    def __truediv__(self, other):
        return type(self)(self._values / other, axes=self.axes)

    def __rtruediv__(self, other):
        return type(self)(other / self._values, axes=self.axes)
    
    def __itruediv__(self, other):
        self.values /= other
        return self
   
    def __floordiv__(self, other):
        return type(self)(self._values // other, axes=self.axes)

    def __rfloordiv__(self, other):
        return type(self)(other // self._values, axes=self.axes)
    
    def __ifloordiv__(self, other):
        self.values //= other
        return self 
    
