"""
tools.py

Author: James Ashton Nichols
Start date: August 2018 

Some tools, including PCA decomposition and some basis generators
"""

import math
import numpy as np
import scipy.linalg
import scipy.sparse

from pyHilbert.space import *
from pyHilbert.vector import *
from pyHilbert.basis import *

