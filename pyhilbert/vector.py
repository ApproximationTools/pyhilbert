"""
vector.py

Author: James Ashton Nichols
Start date: July 2018

The abstraction of some linear algebra in Hilbert spaces for doing functional analysis 
computations, where the class "Basis" does some predictable operations on the 
class "Vector", which has to have a dot product defined as well as typical linear algebra.
This operation is done by having a dictionary of "elements" which contain the simple dot-product
and evaluate routines, as well as 

"""

import math
import numpy as np
import scipy as sp
import collections # For defaultdict

from pyhilbert.pyhilbert.space import *
from pyhilbert.pyhilbert.linop import *

__all__ = ['Vector']

_ORTHO_VECTOR_TOL = 1e-8

class Vector(LinOp):
    """ This is a "reisz" vector, or one that is defined in terms of a set Reisz 
        basis of functions. The main abstraction is for there to be addition, multiplication, and subtraction all to be
        defined to apply to the coefficients of the set Reisz basis functions. From here various function types 
        could inherit, like piece-wise linear functions on general triangulations. Then the benefit is that the 
        Basis or PWBasis can blindly work on any set of PWVector types that are of equivalent class, and it can dot
        product, 
        
        There will necessarily be a dot-product defined between all the functions in the Reisz basis, so the 
        implementation of a dot-product for any class of vector will be simply be the l2 dot product with the symmetric
        positive-definite matrix inside. This is a convenient truth that can make many NumPy operations a whole lot 
        quicker. Also sparsity may well become a useful ally. 
        
        And a final note - we could in fact have PWVector be defined on a "Basis", which kinda makes sense anyhow. In 
        this base-Basis would be P0 or P1 elements on a triangulation for example, or whatever polynomial or wavelet
        system we wish. Then all vectors are defined in terms of this basis, we have the gram-matrix already figured so
        the inner-product is straightforward (and hopefully reasonably sparse). Then it comes down to plotting and 
        evaluating, which could be greatly sped up by "knowing" a bit more about the space spanned by the base-Basis. 
        Ok what exactly do I mean? Something like this:

        v = sum c[i] * h[i]

        i.e. v "has" a basis that it is expressed in, but then the v[i]'s a fundamental and have their dot-product 
        already defined. We might want some sort of interpolation for the various Dyadic levels or for general 
        square-grid refinement... this would require more "knowledge" about the base-Basis than we'd want the basis to 
        know, so yes, maybe we want there to be a sub-type basis that is of square-dyadic type or something like that.

        OK SUPER FINAL LAST NOTE: It seems we define operators -> representers by doing a Galerkin projection often 
        enough (see make_local_avg_grid_basis etc...). This is something a basis of fundamental vectors should be fine 
        doing.

        Ok. Enough ranting - we need to implement all this. """


    def __init__(self, values, space=None, axes=None):
        
        if values is None or (space is None and axes is None):
            raise ValueError(f'{self.__class__.__name__}: Error, can\'t construct '\
                             f'vector with no values or axes/space')

        # Either axes is set or space is set
        if axes:
            if space:
                raise ValueError(f'{self.__class__.__name__}: Must specify either '\
                                 f'axes or space, but not both')
            if len(axes) != 1:
                raise ValueError(f'{self.__class__.__name__}: Axes must be of length 1')

        if space:
            axes = (space,)
        
        super().__init__(values=values, axes=axes)

    @classmethod
    def make_riesz_rep(cls, lin_comb, space):
        return cls(values = space.riesz_rep_coeffs(lin_comb), space = space)

    @property
    def space(self):
        return self.axes[0]

    def _homog(self, other):
        if self.space == other.space:
            return self, other 
        elif other.space.can_interp(self):
            interp = other.space.interp(self)
            return interp, other
        elif self.space.can_interp(other):
            interp = self.space.interp(other)
            return self, interp
        else:
            raise ValueError(f'{self.__class__.__name__}: vector in space '\
                             f'{str(self.space)} is not compatible with vector '\
                             f'in space {str(other.space)}')

    def __matmul__(self, other):        
        return self.matmul(other)

    def __rmatmul__(self, other):
        return self.rmatmul(other)

    def _build_from_matmul(self, other, res_vals, res_axes):
        if hasattr(res_vals, 'ndim'):
            if res_vals.ndim == 1:
                if res_axes[0] is not l2:
                    return Vector(values=res_vals, axes=res_axes)
                else:
                    return res_vals
        
        return super()._build_from_matmul(other, res_vals, res_axes)
        
    def dot(self, other):
        if hasattr(other, 'space') and other.values.ndim == 1:
            # This is explicitly defined to make vec-vec dot prods fast
            sh, oh = self._homog(other)
            return self.space.values_dot(sh.values, oh.values)
        super().dot(other)

    def norm(self):
        return np.sqrt(self.space.values_dot(self.values, self.values))
    
    def orthonormalise(self):
        normed = self / self.norm()
        return normed

    @property
    def is_orthonormal(self):
        if np.abs(self.norm() - 1) <= _ORTHO_VECTOR_TOL:
            return True
        else:
            return False

    def __add__(self, other):
        if hasattr(other, 'space'):
            sh, oh = self._homog(other)
            return type(self)(sh._values + oh._values, space = sh.space)
        elif not isinstance(other, Vector):
            return type(self)(self._values + other, space = self.space)
        else:
            return NotImplemented

    __radd__ = __add__

    def __iadd__(self, other):
        if not isinstance(other, Vector):
            self._values += other
            return self
        sh, oh = self._homog(other)
        self._values = sh._values + oh._values
        self.axes = (sh.space,)
        return self
        
    def __sub__(self, other):
        if hasattr(other, 'space'):
            sh, oh = self._homog(other)
            return type(self)(sh._values - oh._values, space = sh.space)
        elif not isinstance(other, Vector):
            return type(self)(self._values - other, space = self.space)
        else:
            return NotImplemented

    def __isub__(self, other):
        if not isinstance(other, Vector):
            self._values -= other
            return self
        sh, oh = self._homog(other)
        self._values = sh._values - oh._values
        self.axes = (sh.space,)
        return self
    
    def exp(self):
        return type(self)(values = np.exp(self._values), space = self.space)

    def log(self):
        return type(self)(values = np.log(self._values), space = self.space)

    def plot(self, ax, title=None, div_frame=4, alpha=0.5, cmap=None, clim=None, show_axis_labels=True):
        self.space.plot(self, ax, title=title, div_frame=div_frame, alpha=alpha, cmap=cmap, clim=clim, show_axis_labels=True)

    #
    # Factory style constructors 
    #
    @classmethod
    def from_func(cls, func, space):
        x, y = np.meshgrid(space.x_grid, space.y_grid)
        return cls(func(x, y).flatten(), space=space)

    @classmethod
    def make_riesz_rep(cls, lin_comb, space):
        return cls(values = space.riesz_rep_coeffs(lin_comb), space = space)

    @classmethod
    def zero(cls, space):
        return cls(np.zeros(space.n), space=space)
    @classmethod
    def ones(cls, space):
        return cls(np.ones(space.n), space=space)
    
