"""
basis.py

Author: James Ashton Nichols
Start date: July 2018 

The abstraction of some linear algebra in Hilbert spaces for doing functional analysis 
computations, where the class "Basis" does some predictable operations on the 
class "Vector", which has to have a dot product defined as well as typical linear algebra.
"""

import math
import numpy as np
import scipy as sp
import scipy.sparse 
import scipy.linalg
import random
import warnings

import importlib
sksparse_spec = importlib.util.find_spec('sksparse')
_sparse_enabled = sksparse_spec is not None
if sksparse_spec is not None:
    sksparse = importlib.util.module_from_spec(sksparse_spec)

from pyhilbert.pyhilbert.space import *
from pyhilbert.pyhilbert.linop import *
from pyhilbert.pyhilbert.vector import *

__all__ = ['Basis'] 

_ORTHO_MATRIX_TOL = 1e-4
_ORTHO_VECTOR_TOL = 1e-6

class Basis(LinOp):
    """ Class representing the mathematical concept of a Basis. This inherites 
    from LinOp but specifies that at least one axis is l2, meaning that 
    arbitrary linear combinations are possible using matmul() or the @ operator.
    In addition to this some typical routines are provided: 
     - project()
     - orthormalise()
     - apeend()
     - and, iterators to iterate through the vectors
    """

    def __init__(self, values=None, space=None, axes=None, G=None):

        # TODO: We need copy-constructor like behaviour - ability to construct
        # from vector(s) or another Basis... use duck-typing well!
        
        if values is None and space is None and axes is None:
            raise ValueError(f'{self.__class__.__name__}: Error, can\'t construct '\
                              'basis with no values or axes/space')
        
        if isinstance(values, (list, tuple)):
            if space is None and axes is None:
                space = values[0].space
            new_values = np.zeros((values[0].space.n, len(values)))
            for i, v in enumerate(values):
                if hasattr(v, 'values'):
                    v = v.values
                new_values[:,i] = v
            values = new_values

        # TODO: Maybe change this to "space_axis" and make it 0 by default...
        # The logic seems to be a bit backward to what it should be
        self._vector_axis = 1
        # Either axes is set or space is set
        if axes:
            if space:
                raise ValueError(f'{self.__class__.__name__}: Must specify either '\
                                  'axes or space, but not both')
            if len(axes) != 2 and l2 not in axes:
                raise ValueError(f'{self.__class__.__name__}: Axes must be of ' \
                                  'length 2 and contain l2 on one side')
            # axis which is not l2 unless both...
            if axes[0] is l2 and axes[1] is not l2:
                self._vector_axis = 0

        if space:
            axes = (space, l2) # this is default
            
            # I don't think this should really be the behaviour. Deprecating
            #if not space.check_values_shape(values, 0) and space.check_values_shape(values, 1):
            #    axes = (l2, space)
            #    self._vector_axis = 0
        
        super().__init__(values=values, axes=axes)
   
        # CAUTION: We don't at any time check if G is correct. So... make sure it's set correctly
        self._G = G
        if G is None:
            self._vec_is_new = np.full(self.n, True)
        else:
            self._vec_is_new = np.full(self.n, False)

        self._is_orthonormal = None

        self._L_inv = None
        self._U = self._S = self._V = None

    @property
    def n(self):
        return self.values.shape[self._vector_axis]

    def __len__(self):
        return self.n

    @property
    def space(self):
        return self.axes[1 - self._vector_axis]

    def _set_values(self, values):
        
        if values.ndim == 2:
            super()._set_values(values)
        elif values.ndim == 1:
            super()._set_values(values[:, np.newaxis])
        else:
            raise ValueError(f'{self.__class__.__name__}: Error, value array not of dimension 2.')
     
    @property
    def G(self):        
        self.update_G()
        return self._G
    
    def update_G(self):
        if self._G is None:
            self._G = np.zeros((self.n, self.n))
        
        # TODO: Double check the logic of this for NEW bases where we append
        # vectors as we go. I think there's some unpredictable interplay between
        # _is_orthonormal, is_orthonormal, _G and .G
        if np.any(self._vec_is_new):

            vec_selec = (slice(None),) * self._vector_axis + (self._vec_is_new,)
             
            if self._vector_axis == 1:
                new_dots = self.space.values_dot(self._values[vec_selec].T, self._values)
            else:
                new_dots = self.space.values_dot(self._values[vec_selec], self._values.T)
            
            self._G[self._vec_is_new, :] = new_dots
            self._G[:, self._vec_is_new] = new_dots.T
                    
            self._vec_is_new[:] = False

    @property
    def is_orthonormal(self):
        if np.all(~self._vec_is_new): # MAYBE NOT DO THIS AND CALL G ANYHOW
            if self._is_orthonormal is not None:
                return self._is_orthonormal
        
        if np.abs(self.G - np.eye(self.n)).max() / self.n**2 <= _ORTHO_MATRIX_TOL:
            self._is_orthonormal = True
        else:
            self._is_orthonormal = False

        return self._is_orthonormal
 
    @property
    def A(self):
        adj = type(self)(values = self.values.T, axes = self.axes[::-1])
        adj._vector_axis = 1 - self._vector_axis
        return adj

    def __getitem__(self, idx):

        if not isinstance(idx, tuple):
            # We want the default indexing for Basis to be to select vectors of
            # the Basis. It's a little out of step with the row-first indexing
            # that LinOp and NumPy naturally do, but it's used everywhere.

            sel = None
            G_sel = None
            
            vec_selec = (slice(None),) * self._vector_axis + (idx,)
            sel = self._values[vec_selec]

            if self._G is not None and not np.any(self._vec_is_new):
                if isinstance(idx, (slice, int)):
                    G_sel = self._G[idx, idx]
                elif isinstance(idx, (np.ndarray, np.generic)) and len(idx) == len(self) and idx.dtype == np.bool:
                    G_sel = self._G[np.ix_(idx, idx)]

            if sel is not None:
                if sel.ndim > 1: 
                    if sel.shape[0] > 1 and sel.shape[1] > 1:
                        # Somewhat shaky logic here with the adjoint and all these things.
                        # Maybe we need to be agnostic to l2 and adjointness. On the other
                        # hand it's very handy for this indexing method
                        sub = type(self)(values=sel, axes=self.axes, G=G_sel)
                        sub._vector_axis = self._vector_axis
                    else: 
                        # The case where therer is only one vector selected... 
                        # we return the vector, not a basis (should we though ??)
                        sub = Vector(sel.flatten(), space=self.space)
                else:
                    sub = Vector(sel, space=self.space)

                return sub
            else:
                raise TypeError(f'{self.__class__.__name__}: idx type {type(idx)} incorrect')

        else: # If it's a multi-index situation, lets just pass it to the parent
            return super().__getitem__(idx)

    def __setitem__(self, idx, v):
        
        if not isinstance(idx, (tuple, list)):
            # THIS is rather weird: inverted behaviour to select vectors...
            # There's evidently some design issues that I need to think about.
            vec_selec = (slice(None),) * self._vector_axis + (idx,)
        else:
            vec_selec = idx

        # Three cases: we allow v to be a Vector, Basis, or ndarray
        if hasattr(v, 'values') and hasattr(v, 'axes'):
            if v.axes == self.axes or v.space == self.space:
                # Note there is no checking here of space compatibility, but 
                # that mostly will be taken care of by 
                self._values[vec_selec] = v.values

        elif isinstance(v, (list, tuple)):
            for i, vec in enumerate(v):
                i_selec = (slice(None),) * self._vector_axis + (i,)
                self._values[vec_selec][i_selec] = vec.values

        else:
            super().__setitem(idx, v)

    def __iter__(self):
        self._iter_count = 0
        return self

    def __next__(self):
        if self._iter_count < self.n:        
            vec_selec = (slice(None),) * self._vector_axis + (self._iter_count,)
            result = Vector(values=self._values[vec_selec], space=self.space)
            self._iter_count += 1
            return result
        else:
            raise StopIteration

    def norms_sq(self):
        # The default behaviour for a basis is a set of norms along the vec axis
        if self._G is not None and not np.any(self._vec_is_new):
            return np.sqrt(np.diag(self._G))
        elif self.space.G is not None:
            if self._vector_axis == 1:
                mid = self.values * (self.space.G @ self.values)
            else:
                mid = (self.values @ self.space.G) * self.values
        else:
            mid = self.values * self.values

        return mid.sum(axis=1-self._vector_axis)

    def norms(self):
        return np.sqrt(self.norms_sq())

    def dot(self, other):

        return self @ other
        # NOTE THAT THIS USED TO BE A "FAST" SHORTCUT... SHOULD THAT BE REVIVED?
    """if hasattr(other, 'values') and has_attr(other, 'space'):
            other = other.values
            if not other.space == self.space:

        if self.space == other.space:
            dot = self._values @ self.space.G @ other.values
        return dot"""

    def __matmul__(self, other):        
        return self.matmul(other)

    def __rmatmul__(self, other):
        return self.rmatmul(other)

    def _build_from_matmul(self, other, res_vals, res_axes):
        
        if res_vals.ndim == 2 and ((res_axes[0] == l2) ^ (res_axes[1] == l2)):
            return Basis(values=res_vals, axes=res_axes)
        if res_vals.ndim == 1:
            if res_axes[0] != l2:
                return Vector(values=res_vals, axes=res_axes)
            else:
                return res_vals
        
        return super()._build_from_matmul(other, res_vals, res_axes)

    def add_vec(self, vec):
        self.append(vec) 

    def append(self, other, axis=None):
        """ add multiple vectors, no Grammian update """
        # THIS SHOULD PROBABLY BE A CLASS METHOD
        
        if axis is None:
            axis = self._vector_axis
        
        if hasattr(other, 'values') and hasattr(other, 'space'):
            if other.space == self.axes[0] or other.space == self.axes[1]:
                addition = other.values
            #elif isinstance(other, Vector) and self.space.can_interp(other):
            #    addition = self.space.interp(other).values
            # TODO: I think it'd be a good idea to make append require EXPLICIT
            # interpolation, most probably
            else:
                raise ValueError(f'{self.__class__.__name__}: appending vectors '\
                                 f'from incompatible space {str(other.space)}')
        
        elif isinstance(other, (list, tuple)) and all([v.space == self.space for v in other]):
            # Not sure whether to keep supporting this
            addition_n = len(other)
            addition = np.zeros((self.space.n, addition_n)) 
            for i, v in enumerate(other):
                addition[:,i] = v.values
            if self._vector_axis == 0:
                addition = addition.T
        else:
            raise ValueError(f'{self.__class__.__name__}: appending incompatible '\
                             f'type {str(other)}')
   
        if addition.ndim > 1: 
            addition_n = addition.shape[self._vector_axis]
        else:
            addition = addition[:,np.newaxis]
            addition_n = 1

        self._values = np.concatenate((self._values, addition), axis=self._vector_axis)
        # MAYBE CONSIDER ADDING GRAMMIAN CALCS IF SHORT
        if self._G is not None:
            self._G = np.pad(self._G, ((0,addition_n),(0,addition_n)), 'constant')
        self._vec_is_new = np.pad(self._vec_is_new, (0, addition_n), 'constant', constant_values=True)

        self._U = self._V = self._S = None

        return self

    def add_vec_orthogonalise(self, vec):
        """ add a vector - if it is orthonormal already just add it, other wise do one gram-schmidt step,
            which if the basis is already orthogonal, will result in an augmented orthonormal basis """
        vec = vec - self.project(vec)
        norm = vec.norm()
        if norm < _ORTHO_VECTOR_TOL:
            warnings.warn(f'{self.__class__.__name__}: tried adding linearly dependent '\
                           'vector to ortho basis, discarding...')
        else:
            self.append(vec / norm)

    def project(self, u, return_coeffs=False):
        # IDEA: MAYBE IMPLEMENT MATMUL(..., axis=...) to do specific axis mult..
        # USE np.tensordot, make it fast though
        if self._vector_axis == 0:
            c = self @ u
        else:
            c = self.A @ u
        
        return self.dot_to_projection(c, return_coeffs=return_coeffs)

    def dot_to_projection(self, c, return_coeffs=False):
        # Takes the coefficients from a dot product of the basis against a vector
        # and does the least-squares calculation to find the associated (unique)
        # vector.

        if self.is_orthonormal:
            y_n = c
        else:
            try:
                if scipy.sparse.issparse(self.G):
                    y_n = scipy.sparse.linalg.spsolve(self.G, c)
                else:
                    y_n = scipy.linalg.solve(self.G, c, sym_pos=True)
            except np.linalg.LinAlgError as e:
                warnings.warn(f'{self.__class__.__name__}: linearly dependent '\
                               'with {self.n} vectors, projecting using SVD')

                if np.any(self._vec_is_new) or self._U is None or self._S is None or self._V is None:
                    if scipy.sparse.issparse(self.G):
                        self._U, self._S, self._V =  scipy.sparse.linalg.svds(self.G)
                    else:
                        self._U, self._S, self._V = np.linalg.svd(self.G)
                # This is the projection on the reduced rank basis 
                y_n = self._V.T @ ((self._U.T @ c) / self._S)

        # We allow the projection to be of the same type 
        # Also create it from the simple broadcast and sum (which surely should
        # be equivalent to some tensor product thing??)
        if self._vector_axis == 0:
            v = y_n @ self        
        else:
            v = self @ y_n

        if return_coeffs:
            return v, y_n

        return v 

    def orthonormalise(self):

        if self.n == 0:
            return self

        # We do a cholesky factorisation rather than a Gram Schmidt, as
        # we have a symmetric +ve definite matrix, so this is a cheap and
        # easy way to get an orthonormal basis from our previous basis
        try:
            if scipy.sparse.issparse(self.G) and _sparse_enabled:
                L = sksparse.cholmod.cholesky(self.G).L().toarray()
            else:
                L = np.linalg.cholesky(self.G)
        except np.linalg.LinAlgError:
            warnings.warn(f'{self.__class__.__name__}: linear dependence in basis, '\
                           'orthonormalising using PCA')
            sig, V = sp.linalg.eigh(self.G)
            V = V[:,sig > _ORTHO_VECTOR_TOL][:,::-1]
            sig = sig[sig > _ORTHO_VECTOR_TOL][::-1]
            
            return self @ (V / np.sqrt(sig))
        
        self.L_inv = scipy.linalg.lapack.dtrtri(L.T)[0]
               
        if self._vector_axis == 0:
            return self.L_inv.T @ self #orthonormal_basis
        
        return self @ self.L_inv #orthonormal_basis

    def __add__(self, other):
        if self._check_compat(other):
            return type(self)(self.values + other.values, axes=self.axes)

        elif len(other.axes) == 1 and other.axes[0] == self.space:
            # Then it's a vector so we broadcast
            if self._vector_axis == 1:
                return type(self)(self.values + other.values[:,np.newaxis], axes=self.axes)
            else:
                return type(self)(self.values + other.values, axes=self.axes)
                        
    __radd__ = __add__

    def __iadd__(self, other):
        if self._check_compat(other):
            self._values += other.values
            return self

        elif len(other.axes) == 1 and other.axes[0] == self.space:
            # Then it's a vector so we broadcast
            if self._vector_axis == 1:
                self._values += other.values[:,np.newaxis]
            else:
                self._values += other.values

    def __sub__(self, other):
        if self._check_compat(other):
            return type(self)(self.values - other.values, axes=self.axes)

        elif len(other.axes) == 1 and other.axes[0] == self.space:
            # Then it's a vector so we broadcast
            if self._vector_axis == 1:
                return type(self)(self.values - other.values[:,np.newaxis], axes=self.axes)
            else:
                return type(self)(self.values - other.values, axes=self.axes)

    def __rsub__(self, other):
        if self._check_compat(other):
            return type(self)(other.values - self.values, axes=self.axes)

        elif len(other.axes) == 1 and other.axes[0] == self.space:
            # Then it's a vector so we broadcast
            if self._vector_axis == 1:
                return type(self)(other.values[:,np.newaxis] - self.values, axes=self.axes)
            else:
                return type(self)(other.values - self.values, axes=self.axes)

    def __isub__(self, other):
        if self._check_compat(other):
            self._values -= other.values
            #self._vec_is_new[:] = True
            return self

        elif len(other.axes) == 1 and other.axes[0] == self.space:
            # Then it's a vector so we broadcast
            if self._vector_axis == 1:
                self._values -= other.values[:,np.newaxis]
            else:
                self._values -= other.values

    ##
    ## TODO: Possibly deprecate!!
    ##
    def shuffle_vectors(self):
        random.shuffle(self._values)
        self._G = None

    #
    # FACTORY CONSTRUCTORS
    #
    # For creating sinusoidal bases etc...
    #
    @classmethod
    def eye(cls, space):
        # Functions exactly as np.eye but sets up a basis with appropriate
        # space and grammian
        return cls(values = np.eye(space.n), space=space, G=space.G)
       
    @classmethod
    def make_riesz_rep(cls, lin_comb, space):
        return cls(values = space.riesz_rep_coeffs(lin_comb), space = space)

    @classmethod
    def zero(cls, n, space):
        return cls(values = np.zeros((space.n, n)), space = space)

 
